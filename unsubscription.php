<?php
	include ('includes/config.php');
	
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>PHP Mailing list</title> 
	  <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="container">
	<div id="header">
		<a href="index.php"><img src="images/agedman.png" style="height:170px;"/></a>
		<h1>Mailing List Management Page</h1>
	</div>
	<div id="navi">
		<ul>
			<li><a href="subscribe.php">Subscribe to a mailing list</a></li>
			<li><a href="index.php">Add a new mailing list</a></li>
			<li><a href="sendmail.php">Send mail through a list</a></li>
			
		</ul>
	</div><!--end header-->
	
	<div id="maincontent">
		
		<div class="mainstuff">
			<h2>Unsubscribe from mailing list</h2>
			<form>
				<label for="email">Your email address:</label>
				<input type="text" name="email" required="required">
				<br />
				<button type="submit">Unsubscribe</button>
			</form>
		</div>
	
	</div><!--end maincontent-->
</div> <!--end container-->	
	<div id="footer">
		<p>Eetu Kinnunen, Julius Backman, Juuso Virtanen</p>
		<p>14.3.2017</p>
	</div>

<script src="includes/scripts.js" type="text/javascript"></script>
</body>
</html>
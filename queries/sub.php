<?php
include ('../includes/config.php');

ini_set('display_errors', 1);
error_reporting(E_ALL);

/////// Subscribe
if(isset($_POST['email']))
{
		$ID = $_POST['ID'];
		$email = $_POST['email'];
		
		$stmt = $conn->prepare("insert into subscribers (ref_list, subscriber) values (?, ?)");
		$stmt->bind_param("ss", $ID, $email);
		
		if($stmt->execute())
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert(\"subscribed successfully\")
			window.location.href='../index.php'
			</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert(\"cant subscrib.\")
			window.location.href='../subscribe.php'
			</SCRIPT>");
		}		

		$stmt->close();
}
?>
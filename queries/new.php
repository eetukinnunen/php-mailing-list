<?php
include ('../includes/config.php');

ini_set('display_errors', 1);
error_reporting(E_ALL);

/////// New mailing list
if(isset($_POST['listname']) and isset($_POST['email']))
{
	if ($_POST['listname']=='' or $_POST['email'] ==''){
		echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.alert(\"Please fill in desired list name and email address.\")
        window.location.href='../index.php'
        </SCRIPT>");
	}
	else{
		$listname = $_POST['listname'];
		$email = $_POST['email'];
		$bytes = (bin2hex(random_bytes(5)));
		
		$stmt = $conn->prepare("insert into list (owner, listname, randomstring) values (?, ?, ?)");
		$stmt->bind_param("sss", $email, $listname, $bytes);
		
		if($stmt->execute()){
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert(\"List ".$_POST['listname']." created successfully!\")
			window.location.href='../index.php'
			</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert(\"List of this name already exists. Try using a different name.\")
			window.location.href='../index.php'
			</SCRIPT>");
		}
		// Vaihan alertin johonkin parempaan ratkaisuun joskus
		
		$stmt->close();
	}
}

?>
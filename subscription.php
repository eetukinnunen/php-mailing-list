<?php
	include ('includes/config.php');
	
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	if(!isset($_GET['ID']))
	{
		header('Location: subscribe.php');
	}
	
	if(!is_numeric($_GET['ID']))
	{
		header('Location: subscribe.php');
	}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>PHP Mailing list</title> 
	  <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="container">
	<div id="header">
		<a href="index.php"><img src="images/oldman.png" style="height:170px;"/></a>
		<h1>Mailing List Management Page</h1>
	</div>
	<div id="navi">
		<ul>
			<li><a href="subscribe.php">Subscribe to a mailing list</a></li>
			<li><a href="index.php">Add a new mailing list</a></li>
			<li><a href="sendmail.php">Send mail through a list</a></li>
			
		</ul>
	</div><!--end header-->
	
	<div id="maincontent">
		<div class="mainstuff">
			<form name="create" action="queries/sub.php" onsubmit="return validateForm();" method="post">
				<input type="hidden" name="ID" value="<?php echo $_GET['ID']; ?>">
				<?php
					echo "<h2>Subscribe</h2>";
					$result = mysqli_query($conn, "SELECT * FROM list WHERE ID=".$_GET['ID']."");
					while ($row = mysqli_fetch_array($result))
					{
						echo "<p>";
						echo "<p>List name: ". $row['listname']."</p>";
						echo "</p>";
						echo "<p>List owner: ". $row['owner']."</p>";
						echo "</p>";
					}
				?>
				<label for="email">Your email address:</label>
				<input type="text" name="email" required="required" />
				<button type="submit">Subscribe</button>
			</form>
		</div><!--end mainstuff-->
	</div><!--end maincontent-->
</div> <!--end container-->	
	<div id="footer">
		<p>Eetu Kinnunen, Julius Backman, Juuso Virtanen</p>
		<p>14.3.2017</p>
	</div>

<script src="includes/scripts.js" type="text/javascript"></script>
</body>
</html>
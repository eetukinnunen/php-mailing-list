// Displays the thing when you choose "add a new mailing list"
window.onload = function() {

          var a = document.getElementById("addlist");

          a.onclick = function() {
			  
			  document.getElementById("createlist").style.display = "block"; 
			  return false;
          }
        }
		
// Email validation
function validateForm() {
    var x = document.forms["create"]["email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid email address.");
        return false;
    }
}
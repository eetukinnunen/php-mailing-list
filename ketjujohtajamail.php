<?php
	include ('includes/config.php');
	
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	
	if(!isset($_GET['randomstring']))
	{
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>PHP Mailing list</title> 
	  <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="container">
	<div id="header">
		<a href="index.php"><img src="images/elderlyman.png" style="height:170px;"/></a>
		<h1>Mailing List Management Page</h1>
	</div>
	<div id="navi">
		<ul>
			<li><a href="subscribe.php">Subscribe to a mailing list</a></li>
			<li><a href="index.php">Add a new mailing list</a></li>
			<li><a href="sendmail.php">Send mail through a list</a></li>			
		</ul>
	</div><!--end header-->
	
	<div id="maincontent">
		<div class="mainstuff">
		<input type="hidden" name="randomstring" value="<?php echo $_GET['randomstring']; ?>">
			<?php
				$random = $_GET['randomstring'];
				
				$res = mysqli_query($conn, "SELECT listname FROM list WHERE randomstring = '$random'");
				while ($row = mysqli_fetch_array($res))
					{
						echo "<h2>". $row['listname']."</h2>";
					}
				echo "<p>Subscribers:</p>";
				$result = mysqli_query($conn, "SELECT * FROM subscribers WHERE ref_list = (SELECT ID FROM list WHERE randomstring = '$random')");
				while ($row = mysqli_fetch_array($result))
					{
						echo "<p>". $row['subscriber']."</p>";
					}
			?>
			<br />
			<form method="post" action="queries/subsend.php">
				<p>Subject:<br />
					<textarea name="masstitle" cols="60" rows="1"></textarea>
				</p>
				<p>Message:<br />
					<textarea name="massemail" cols="60" rows="20"></textarea>
				</p>
				<input type="submit" value="Send">
			</form>
		</div><!--end mainstuff-->
	</div><!--end maincontent-->
</div> <!--end container-->	
	<div id="footer">
		<p>Eetu Kinnunen, Julius Backman, Juuso Virtanen</p>
		<p>14.3.2017</p>
	</div>

<script src="includes/scripts.js" type="text/javascript"></script>
</body>
</html>
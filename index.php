<?php
	include ('includes/config.php');
	
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>PHP Mailing list</title> 
	  <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="container">
	<div id="header">
		<a href="index.php"><img src="images/seniorman.png" style="height:170px;"/></a>
		<h1>Mailing List Management Page</h1>
	</div>
	<div id="navi">
		<ul>
			<li><a href="subscribe.php">Subscribe to a mailing list</a></li>
			<li><a id="addlist" href="#">Add a new mailing list</a></li>
			<li><a href="sendmail.php">Send mail through a list</a></li>
			
		</ul>
	</div><!--end header-->
	
	<div id="maincontent">
	
		<div id="createlist">
			<h2>New mailing list</h2>
			<form name="create" action="queries/new.php" onsubmit="return validateForm();" method="post">
				<label for="listname">Name of the list:</label>
				<input type="text" name="listname" required="required" />
					<br></br>
				<label for="email">Your email address:</label>
				<input type="text" name="email" required="required" />
					<br></br>
				<button type="submit">Create mailing list</button>
			</form>
		</div>
		
		<div class="mainstuff">
			<p>
				Welcome to the Mailing List Management Page!<br />
				Here you can:
			</p>
			<ul>
				<li>Subscribe to any existing mailing list to receive emails from them</li>
				<li>Make a new mailing list that others can subscribe to</li>
				<li>Send mail to all your list's subscribers at once</li>
			</ul>
			<p>
				
			</p>
			<p id="course">
				<br />Web Application Development<br />PHP assignment
			</p>
		</div>
	
	</div><!--end maincontent-->
</div> <!--end container-->	
	<div id="footer">
		<p>Eetu Kinnunen, Julius Backman, Juuso Virtanen</p>
		<p>14.3.2017</p>
	</div>

<script src="includes/scripts.js" type="text/javascript"></script>
</body>
</html>